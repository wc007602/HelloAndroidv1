package com.example.owner.helloandroid;

import java.util.Random;

/**
 * Created by Owner on 23/01/2018.
 */

public class Guess {

    public int valueToGuess;
    public int maxValue;
    public String hint;
    private Random rand = new Random();     // random generator

    // counter for each turn
    public int guessCount;
    public int guessLimit;   // limit for how many guesses per game the user can make

    /**
     * default constructor
     */
    public Guess(){
        this.maxValue = 10;
        this.hint = null;
        this.guessCount = 0;
        this.guessLimit = 5;

        setNewValue();
    }

    /**
     * constructor for guess with parameters
     */
    public Guess(int valueToGuess, int guessCount, String hint){
        this.valueToGuess = valueToGuess;
        this.maxValue = 10;
        this.hint = hint;
        this.guessCount = guessCount;
        this.guessLimit = 5;
    }

    /**
     * method to set new value for valueToGuess
     */
    public void setNewValue(){
        // declare valueToGuess here
        valueToGuess = 1+rand.nextInt(maxValue-1);
        System.out.println(valueToGuess);
    }

    /**
     * this function is called in a loop until the user inputs X as the correct value
     * @param X
     * @return true if guess X == value, else return false
     */
    public boolean makeGuess(int X){
        if(X == valueToGuess){
            System.out.println("CORRECT.");
            return true;
        } else {
            System.out.println("wrong");

            // now set hint to say higher or lower
            if(X > valueToGuess) {
                hint = "lower";
            } else if(X < valueToGuess){
                hint = "greater";
            }

            guessCount++;
            return false;
        }
    }

    /**
     * checks whether the guessCount variable has been reached
     * @return true if limit reached, return false otherwise
     */
    public boolean checkGuessCount(){
        if(guessCount >= guessLimit){
            return true;
        }else{
            return false;
        }
    }

}
