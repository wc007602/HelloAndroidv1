package com.example.owner.helloandroid;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.content.Intent;

import java.io.IOError;

public class MainActivity extends AppCompatActivity {

    // bring in the Guess class
    private Guess gs;

    static int scoreCount = 0;

    // two states for the button state
    enum BTNSTATE {GUESS, GUESSAGAIN, RESTART};
    BTNSTATE thisState = BTNSTATE.GUESS;

    String STATE_VALUE = "valueToGuess";
    String STATE_COUNT = "playerCount";
    String STATE_HINT = "playerHint";

    // UI elements
    Button guessBtn;
    Button scoresBtn;
    TextView mainTextView;
    EditText inputEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // if saveInstance is not empty
        if (savedInstanceState != null) {
            int v = savedInstanceState.getInt(STATE_VALUE);
            int c = savedInstanceState.getInt(STATE_COUNT);
            String h = savedInstanceState.getString(STATE_HINT);

            // now create the state
            gs = new Guess(v, c, h);

        } else{
            // else set default values
            gs = new Guess();
        }

        setup();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);

        int v = savedInstanceState.getInt(STATE_VALUE);
        int c = savedInstanceState.getInt(STATE_COUNT);
        String h = savedInstanceState.getString(STATE_HINT);

        gs = new Guess(v, c, h);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        savedInstanceState.putInt(STATE_VALUE, gs.valueToGuess);
        savedInstanceState.putInt(STATE_COUNT, gs.guessCount);
        savedInstanceState.putString(STATE_HINT, gs.hint);

        super.onSaveInstanceState(savedInstanceState);
    }


    /**
     * function to setup and run the application
     */
    protected void setup(){

        // setup UI elements
        guessBtn = (Button)findViewById(R.id.BTN_guess);
        scoresBtn = (Button)findViewById(R.id.BTN_scores);
        mainTextView = (TextView)findViewById(R.id.TV_main);
        inputEditText = (EditText)findViewById(R.id.ET_input);

        // set the start text for a new game
        mainTextView.setText(getResources().getString(R.string.tv_starttext,gs.maxValue));

        // initialise the button
        setButtonState(thisState);

        // checks if the button is being clicked
        guessBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // first pass input as string so can compare to null
                String textInput = inputEditText.getText().toString();

                // if null is not entered
                if(textInput.isEmpty() != true){
                    int numInput = Integer.parseInt(textInput);

                    if(gs.makeGuess(numInput) == true){
                        // if user guessing correct
                        mainTextView.setText(getResources().getString(R.string.tv_correct));
                        thisState = BTNSTATE.RESTART;
                        setButtonState(thisState);

                        // also need to save score to preferences
                        updateScores(gs);

                    } else if(gs.checkGuessCount() == false){
                        // otherwise try again
                        mainTextView.setText(getResources().getString(R.string.tv_guessagain, gs.hint, numInput, (gs.guessLimit - gs.guessCount)));
                        thisState = BTNSTATE.GUESSAGAIN;
                        setButtonState(thisState);

                    } else {
                        // if you have hit the limit, offer to restart game
                        mainTextView.setText(getResources().getString(R.string.tv_limitreached));
                        thisState = BTNSTATE.RESTART;
                        setButtonState(thisState);
                    }
                }

                else {
                    // if BTNSTATE is RESTART
                    if(thisState == BTNSTATE.RESTART){
                        // reset the game with a new number
                        gs = new Guess();
                        mainTextView.setText(getResources().getString(R.string.tv_starttext, gs.maxValue));
                        thisState = BTNSTATE.GUESS;
                        setButtonState(thisState);
                    } else{
                        // otherwise ask to input number
                        mainTextView.setText(getResources().getString(R.string.tv_noinput));
                        System.out.println("NULL");
                    }
                }

                // remove the last guess
                inputEditText.setText(null);
            }
        });

        scoresBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewScores();
            }
        });

    }

    /**
     * sets the text of the button depending on the state of the enum
     * @param state
     */
    protected void setButtonState(BTNSTATE state){

        // if user is making a guess, change the text
        if(state==BTNSTATE.GUESS){
            guessBtn.setText(getResources().getString(R.string.btn_guess));
        } else if(state==BTNSTATE.GUESSAGAIN){
            // otherwise user is trying again
            guessBtn.setText(getResources().getString(R.string.btn_guessagain));
        } else if(state==BTNSTATE.RESTART){
            guessBtn.setText(getResources().getString(R.string.btn_playagain));
        } else{
            System.out.println("ERROR IN setButtonState");
        }

    }

    /**
     * update the scores every time a game is won
     */
    protected void updateScores(Guess gs){
        scoreCount++;

        // also need to save score to preferences
        SharedPreferences settings = getSharedPreferences(getResources().getString(R.string.PREFS_NAME), 0);
        SharedPreferences.Editor editor = settings.edit();

        int score = (gs.guessLimit - gs.guessCount)*10;

        switch (scoreCount){
            case 1:
                editor.putInt(getResources().getString(R.string.KEY_SCORE1), score);
                break;
            case 2:
                editor.putInt(getResources().getString(R.string.KEY_SCORE2), score);
                break;
            case 3:
                editor.putInt(getResources().getString(R.string.KEY_SCORE3), score);
                break;
            case 4:
                editor.putInt(getResources().getString(R.string.KEY_SCORE4), score);
                break;
            case 5:
                editor.putInt(getResources().getString(R.string.KEY_SCORE5), score);
                break;
                default:
                    // find lowest score and replacee if lower than current score variable
                    break;
        }

        editor.commit();
    }

    /**
     * switch activities to view user scores
     */
    public void viewScores(){
        Intent scoreIntent = new Intent(this, SavedScores.class);
        startActivity(scoreIntent);
    }

}
