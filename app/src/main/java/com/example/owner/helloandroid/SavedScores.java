package com.example.owner.helloandroid;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class SavedScores extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_scores);

        setup();
    }

    protected void setup(){
        // add the text view items to display scores
        TextView score1 = (TextView)findViewById(R.id.SCORE_1);
        TextView score2 = (TextView)findViewById(R.id.SCORE_2);
        TextView score3 = (TextView)findViewById(R.id.SCORE_3);
        TextView score4 = (TextView)findViewById(R.id.SCORE_4);
        TextView score5 = (TextView)findViewById(R.id.SCORE_5);

        SharedPreferences settings = getSharedPreferences(getResources().getString(R.string.PREFS_NAME), 0);
        int one = settings.getInt(getResources().getString(R.string.KEY_SCORE1), 0);
        int two = settings.getInt(getResources().getString(R.string.KEY_SCORE2), 0);
        int three = settings.getInt(getResources().getString(R.string.KEY_SCORE3), 0);
        int four = settings.getInt(getResources().getString(R.string.KEY_SCORE4), 0);
        int five = settings.getInt(getResources().getString(R.string.KEY_SCORE3), 0);

        score1.setText(getResources().getString(R.string.display_score1, one));
        score2.setText(getResources().getString(R.string.display_score2, two));
        score3.setText(getResources().getString(R.string.display_score3, three));
        score4.setText(getResources().getString(R.string.display_score4, three));
        score5.setText(getResources().getString(R.string.display_score5, three));



    }
}
